#include "estatistica.h"
#include <stdio.h>
#define MAX 256

int main() {
  int n;
  scanf("%d", &n);
  double v[MAX], m;

  for (int i = 0; i < n; i++) {
    scanf("%lf", &v[i]);
  }

  m = media(n, v);

  printf("%0.2lf\n", m);

  return 0;
}

