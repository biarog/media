#include <stdio.h>

double media(int n, double *v) {
  
  double m = 0;
  
  for (int i = 0; i < n; i++) {
    m += v[i];
  }
  
  m /= n;
  
  return m;
}
