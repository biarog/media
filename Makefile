media: media.o estatistica.o
	gcc media.o estatistica.o -o media

estatistica.o: estatistica.c estatistica.h
	gcc -c estatistica.c -o estatistica.o

media.o: media.c estatistica.h
	gcc -c media.c -o media.o

clean:
	rm media *.o
